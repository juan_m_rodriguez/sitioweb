package co.org.invemar.springapp.service;

import java.util.List;

import co.org.invemar.springapp.domain.Product;

public interface ProductManager {

	public void increasePrice(int percentage);
	public List <Product> getProducts();
}
